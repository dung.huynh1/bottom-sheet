"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useBottomSheetModalInternal = void 0;

var _react = require("react");

var _contexts = require("../contexts");

const useBottomSheetModalInternal = () => (0, _react.useContext)(_contexts.BottomSheetModalInternalContext);

exports.useBottomSheetModalInternal = useBottomSheetModalInternal;
//# sourceMappingURL=useBottomSheetModalInternal.js.map