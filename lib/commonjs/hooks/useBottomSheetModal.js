"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useBottomSheetModal = void 0;

var _react = require("react");

var _external = require("../contexts/modal/external");

const useBottomSheetModal = () => (0, _react.useContext)(_external.BottomSheetModalContext);

exports.useBottomSheetModal = useBottomSheetModal;
//# sourceMappingURL=useBottomSheetModal.js.map