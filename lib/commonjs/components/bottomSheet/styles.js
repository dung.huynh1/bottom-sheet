"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = void 0;

var _reactNative = require("react-native");

const styles = _reactNative.StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0
  },
  contentContainer: {},
  contentMaskContainer: {
    overflow: 'hidden'
  }
});

exports.styles = styles;
//# sourceMappingURL=styles.js.map