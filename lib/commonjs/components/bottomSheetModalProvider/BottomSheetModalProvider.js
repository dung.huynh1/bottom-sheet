"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _portal = require("@gorhom/portal");

var _contexts = require("../../contexts");

var _bottomSheetContainer = _interopRequireDefault(require("../bottomSheetContainer"));

var _constants = require("../../constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const BottomSheetModalProviderWrapper = ({
  children
}) => {
  //#region layout state
  const [containerHeight, setContainerHeight] = (0, _react.useState)(_constants.WINDOW_HEIGHT); //#endregion
  //#region variables

  const sheetsQueueRef = (0, _react.useRef)([]); //#endregion
  //#region callback

  const handleOnContainerMeasureHeight = (0, _react.useCallback)(height => {
    setContainerHeight(height);
  }, []); //#endregion
  //#region private methods

  const handleMountSheet = (0, _react.useCallback)((key, ref) => {
    const _sheetsQueue = sheetsQueueRef.current.slice();

    const sheetIndex = _sheetsQueue.findIndex(item => item.key === key);

    const sheetOnTop = sheetIndex === _sheetsQueue.length - 1;
    /**
     * Exit the method, if sheet is already presented
     * and at the top.
     */

    if (sheetIndex !== -1 && sheetOnTop) {
      return;
    }
    /**
     * Minimize the current sheet if:
     * - it exists.
     * - it is not unmounting.
     */


    const currentMountedSheet = _sheetsQueue[_sheetsQueue.length - 1];

    if (currentMountedSheet && !currentMountedSheet.willUnmount) {
      currentMountedSheet.ref.current.minimize();
    }
    /**
     * Restore and remove incoming sheet from the queue,
     * if it was registered.
     */


    if (sheetIndex !== -1) {
      _sheetsQueue.splice(sheetIndex, 1);

      ref.current.restore();
    }

    _sheetsQueue.push({
      key,
      ref,
      willUnmount: false
    });

    sheetsQueueRef.current = _sheetsQueue;
  }, []);
  const handleUnmountSheet = (0, _react.useCallback)(key => {
    const _sheetsQueue = sheetsQueueRef.current.slice();

    const sheetIndex = _sheetsQueue.findIndex(item => item.key === key);

    const sheetOnTop = sheetIndex === _sheetsQueue.length - 1;
    /**
     * Here we remove the unmounted sheet and update
     * the sheets queue.
     */

    _sheetsQueue.splice(sheetIndex, 1);

    sheetsQueueRef.current = _sheetsQueue;
    /**
     * Here we try to restore previous sheet position if unmounted
     * sheet was on top. This is needed when user dismiss
     * the modal by panning down.
     */

    const hasMinimizedSheet = sheetsQueueRef.current.length > 0;

    if (sheetOnTop && hasMinimizedSheet) {
      sheetsQueueRef.current[sheetsQueueRef.current.length - 1].ref.current.restore();
    }
  }, []);
  const handleWillUnmountSheet = (0, _react.useCallback)(key => {
    const _sheetsQueue = sheetsQueueRef.current.slice();

    const sheetIndex = _sheetsQueue.findIndex(item => item.key === key);

    const sheetOnTop = sheetIndex === _sheetsQueue.length - 1;
    /**
     * Here we mark the sheet that will unmount,
     * so it won't be restored.
     */

    if (sheetIndex !== -1) {
      _sheetsQueue[sheetIndex].willUnmount = true;
    }
    /**
     * Here we try to restore previous sheet position,
     * This is needed when user dismiss the modal by fire the dismiss action.
     */


    const hasMinimizedSheet = _sheetsQueue.length > 1;

    if (sheetOnTop && hasMinimizedSheet) {
      _sheetsQueue[_sheetsQueue.length - 2].ref.current.restore();
    }

    sheetsQueueRef.current = _sheetsQueue;
  }, []); //#endregion
  //#region public methods

  const handleDismiss = (0, _react.useCallback)(key => {
    const sheetToBeDismissed = sheetsQueueRef.current.find(item => item.key === key);

    if (sheetToBeDismissed) {
      sheetToBeDismissed.ref.current.dismiss(true);
    }
  }, []);
  const handleDismissAll = (0, _react.useCallback)(() => {
    sheetsQueueRef.current.map(item => {
      item.ref.current.dismiss(true);
    });
  }, []); //#endregion
  //#region context variables

  const externalContextVariables = (0, _react.useMemo)(() => ({
    dismiss: handleDismiss,
    dismissAll: handleDismissAll
  }), [handleDismiss, handleDismissAll]);
  const internalContextVariables = (0, _react.useMemo)(() => ({
    containerHeight,
    mountSheet: handleMountSheet,
    unmountSheet: handleUnmountSheet,
    willUnmountSheet: handleWillUnmountSheet
  }), [containerHeight, handleMountSheet, handleUnmountSheet, handleWillUnmountSheet]); //#endregion
  //#region renders

  return /*#__PURE__*/_react.default.createElement(_contexts.BottomSheetModalProvider, {
    value: externalContextVariables
  }, /*#__PURE__*/_react.default.createElement(_contexts.BottomSheetModalInternalProvider, {
    value: internalContextVariables
  }, /*#__PURE__*/_react.default.createElement(_bottomSheetContainer.default, {
    shouldMeasureHeight: true,
    onMeasureHeight: handleOnContainerMeasureHeight,
    children: null
  }), /*#__PURE__*/_react.default.createElement(_portal.PortalProvider, null, children))); //#endregion
};

var _default = BottomSheetModalProviderWrapper;
exports.default = _default;
//# sourceMappingURL=BottomSheetModalProvider.js.map