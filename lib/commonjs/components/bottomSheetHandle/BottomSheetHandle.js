"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _lodash = _interopRequireDefault(require("lodash.isequal"));

var _styles = require("./styles");

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const BottomSheetHandleComponent = props => {
  const {
    accessible = _constants.DEFAULT_ACCESSIBLE,
    accessibilityRole = _constants.DEFAULT_ACCESSIBILITY_ROLE,
    accessibilityLabel = _constants.DEFAULT_ACCESSIBILITY_LABEL,
    accessibilityHint = _constants.DEFAULT_ACCESSIBILITY_HINT,
    ...rest
  } = props;
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, _extends({
    style: _styles.styles.container,
    accessible: accessible !== null && accessible !== void 0 ? accessible : undefined,
    accessibilityRole: accessibilityRole !== null && accessibilityRole !== void 0 ? accessibilityRole : undefined,
    accessibilityLabel: accessibilityLabel !== null && accessibilityLabel !== void 0 ? accessibilityLabel : undefined,
    accessibilityHint: accessibilityHint !== null && accessibilityHint !== void 0 ? accessibilityHint : undefined
  }, rest), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: _styles.styles.indicator
  }));
};

const BottomSheetHandle = /*#__PURE__*/(0, _react.memo)(BottomSheetHandleComponent, _lodash.default);
var _default = BottomSheetHandle;
exports.default = _default;
//# sourceMappingURL=BottomSheetHandle.js.map