"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = void 0;

var _reactNative = require("react-native");

const styles = _reactNative.StyleSheet.create({
  container: {
    position: 'absolute',
    left: 4,
    top: 4,
    padding: 2,
    backgroundColor: 'rgba(0, 0,0,0.75)'
  },
  text: {
    fontSize: 16,
    lineHeight: 20,
    height: 20,
    padding: 0,
    color: 'white'
  }
});

exports.styles = styles;
//# sourceMappingURL=styles.js.map