"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usePressBehavior = void 0;

var _react = require("react");

var _hooks = require("../../hooks");

var _constants = require("./constants");

const usePressBehavior = ({
  closeOnPress,
  disappearsOnIndex,
  pressBehavior
}) => {
  //#region hooks
  const {
    snapTo,
    close
  } = (0, _hooks.useBottomSheet)(); //#endregion
  //#region variables

  const syntheticPressBehavior = (0, _react.useMemo)(() => {
    if (typeof closeOnPress === 'boolean') {
      return closeOnPress ? 'close' : 'none';
    }

    if (closeOnPress === -1) {
      return 'close';
    }

    if (closeOnPress === disappearsOnIndex) {
      return 'collapse';
    }

    return pressBehavior !== null && pressBehavior !== void 0 ? pressBehavior : _constants.DEFAULT_PRESS_BEHAVIOR;
  }, [pressBehavior, closeOnPress, disappearsOnIndex]);
  const handleOnPress = (0, _react.useCallback)(() => {
    if (syntheticPressBehavior === 'close') {
      close();
    } else if (syntheticPressBehavior === 'collapse') {
      snapTo(disappearsOnIndex);
    } else if (typeof syntheticPressBehavior === 'number') {
      snapTo(syntheticPressBehavior);
    }
  }, [close, disappearsOnIndex, syntheticPressBehavior, snapTo]); //#endregion
  //#region effects

  __DEV__ && // eslint-disable-next-line react-hooks/rules-of-hooks
  (0, _react.useEffect)(() => {
    if (closeOnPress != null && pressBehavior != null) {
      console.warn('BottomSheetBackdrop: you should never define both closeOnPress and pressBehavior. closeOnPress will take precedence.');
    }
  }, [closeOnPress, pressBehavior]); //#endregion

  return {
    handleOnPress,
    syntheticPressBehavior
  };
};

exports.usePressBehavior = usePressBehavior;
//# sourceMappingURL=usePressBehavior.js.map