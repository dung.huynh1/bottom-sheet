"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _lodash = _interopRequireDefault(require("lodash.isequal"));

var _styles = require("./styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const BottomSheetContainerComponent = ({
  shouldMeasureHeight,
  onMeasureHeight,
  children,
  topInset = 0,
  bottomInset = 0
}) => {
  //#region callbacks
  const handleOnLayout = (0, _react.useCallback)(({
    nativeEvent: {
      layout: {
        height
      }
    }
  }) => {
    onMeasureHeight(height);
  }, [onMeasureHeight]); //#endregion

  const containerStyle = (0, _react.useMemo)(() => [_styles.styles.container, {
    top: topInset,
    bottom: bottomInset
  }], [bottomInset, topInset]); //#region render
  // console.log('BottomSheetContainer', 'render', shouldMeasureHeight);

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    pointerEvents: "box-none",
    style: containerStyle,
    onLayout: shouldMeasureHeight ? handleOnLayout : undefined,
    children: children
  }); //#endregion
};

const BottomSheetContainer = /*#__PURE__*/(0, _react.memo)(BottomSheetContainerComponent, _lodash.default);
var _default = BottomSheetContainer;
exports.default = _default;
//# sourceMappingURL=BottomSheetContainer.js.map