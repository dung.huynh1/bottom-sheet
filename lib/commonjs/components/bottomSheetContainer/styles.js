"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = void 0;

var _reactNative = require("react-native");

const styles = _reactNative.StyleSheet.create({
  container: { ..._reactNative.StyleSheet.absoluteFillObject,
    overflow: 'hidden'
  }
});

exports.styles = styles;
//# sourceMappingURL=styles.js.map