import { createContext } from 'react';
// @ts-ignore
export const BottomSheetModalContext = /*#__PURE__*/createContext();
export const BottomSheetModalProvider = BottomSheetModalContext.Provider;
//# sourceMappingURL=external.js.map