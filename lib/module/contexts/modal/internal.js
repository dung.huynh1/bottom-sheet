import { createContext } from 'react';
// @ts-ignore
export const BottomSheetModalInternalContext = /*#__PURE__*/createContext();
export const BottomSheetModalInternalProvider = BottomSheetModalInternalContext.Provider;
//# sourceMappingURL=internal.js.map