function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { forwardRef, memo, useCallback, useImperativeHandle, useMemo, useRef, useState } from 'react';
import { Portal, usePortal } from '@gorhom/portal';
import { nanoid } from 'nanoid/non-secure';
import isEqual from 'lodash.isequal';
import BottomSheet from '../bottomSheet';
import { useBottomSheetModalInternal } from '../../hooks';
import { DEFAULT_DISMISS_ON_PAN_DOWN } from './constants';
const BottomSheetModalComponent = /*#__PURE__*/forwardRef((props, ref) => {
  const {
    // modal props
    name,
    dismissOnPanDown = DEFAULT_DISMISS_ON_PAN_DOWN,
    onDismiss: _providedOnDismiss,
    // bottom sheet props
    index: _providedIndex = 0,
    snapPoints: _providedSnapPoints,
    onChange: _providedOnChange,
    topInset = 0,
    bottomInset = 0,
    // components
    children,
    ...bottomSheetProps
  } = props; //#region state

  const [mount, setMount] = useState(false); //#endregion
  //#region hooks

  const {
    containerHeight,
    mountSheet,
    unmountSheet,
    willUnmountSheet
  } = useBottomSheetModalInternal();
  const {
    removePortal: unmountPortal
  } = usePortal(); //#endregion
  //#region refs

  const bottomSheetRef = useRef(null);
  const currentIndexRef = useRef(-1);
  const restoreIndexRef = useRef(-1);
  const minimized = useRef(false);
  const forcedDismissed = useRef(false);
  const mounted = useRef(true); //#endregion
  //#region variables

  const key = useMemo(() => name || "bottom-sheet-modal-".concat(nanoid()), [name]);
  const index = useMemo(() => dismissOnPanDown ? _providedIndex + 1 : _providedIndex, [_providedIndex, dismissOnPanDown]);
  const snapPoints = useMemo(() => dismissOnPanDown ? [0, ..._providedSnapPoints] : _providedSnapPoints, [_providedSnapPoints, dismissOnPanDown]);
  const safeContainerHeight = useMemo(() => containerHeight - topInset - bottomInset, [containerHeight, topInset, bottomInset]); //#endregion
  //#region private methods

  const resetVariables = useCallback(() => {
    currentIndexRef.current = -1;
    restoreIndexRef.current = -1;
    minimized.current = false;
    mounted.current = true;
    forcedDismissed.current = false;
  }, []);
  const adjustIndex = useCallback(_index => dismissOnPanDown ? _index - 1 : _index, [dismissOnPanDown]);
  const unmount = useCallback(() => {
    const _mounted = mounted.current; // reset variables

    resetVariables(); // unmount sheet and portal

    unmountSheet(key);
    unmountPortal(key); // unmount the node, if sheet is still mounted

    if (_mounted) {
      setMount(false);
    } // fire `onDismiss` callback


    if (_providedOnDismiss) {
      _providedOnDismiss();
    }
  }, [key, resetVariables, unmountSheet, unmountPortal, _providedOnDismiss]); //#endregion
  //#region bottom sheet methods

  const handleSnapTo = useCallback(_index => {
    var _bottomSheetRef$curre;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre = bottomSheetRef.current) === null || _bottomSheetRef$curre === void 0 ? void 0 : _bottomSheetRef$curre.snapTo(adjustIndex(_index));
  }, [adjustIndex]);
  const handleExpand = useCallback(() => {
    var _bottomSheetRef$curre2;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre2 = bottomSheetRef.current) === null || _bottomSheetRef$curre2 === void 0 ? void 0 : _bottomSheetRef$curre2.expand();
  }, []);
  const handleCollapse = useCallback(() => {
    if (minimized.current) {
      return;
    }

    if (dismissOnPanDown) {
      var _bottomSheetRef$curre3;

      (_bottomSheetRef$curre3 = bottomSheetRef.current) === null || _bottomSheetRef$curre3 === void 0 ? void 0 : _bottomSheetRef$curre3.snapTo(1);
    } else {
      var _bottomSheetRef$curre4;

      (_bottomSheetRef$curre4 = bottomSheetRef.current) === null || _bottomSheetRef$curre4 === void 0 ? void 0 : _bottomSheetRef$curre4.collapse();
    }
  }, [dismissOnPanDown]);
  const handleClose = useCallback(() => {
    var _bottomSheetRef$curre5;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre5 = bottomSheetRef.current) === null || _bottomSheetRef$curre5 === void 0 ? void 0 : _bottomSheetRef$curre5.close();
  }, []); //#endregion
  //#region bottom sheet modal methods

  const handlePresent = useCallback(() => {
    requestAnimationFrame(() => {
      setMount(true);
      mountSheet(key, ref);
    });
  }, [key, ref, mountSheet]);
  const handleDismiss = useCallback(() => {
    var _bottomSheetRef$curre6;

    /**
     * if modal is already been dismiss, we exit the method.
     */
    if (currentIndexRef.current === -1 && minimized.current === false) {
      return;
    }

    if (minimized.current) {
      unmount();
      return;
    }

    willUnmountSheet(key);
    forcedDismissed.current = true;
    (_bottomSheetRef$curre6 = bottomSheetRef.current) === null || _bottomSheetRef$curre6 === void 0 ? void 0 : _bottomSheetRef$curre6.close();
  }, [willUnmountSheet, unmount, key]);
  const handleMinimize = useCallback(() => {
    var _bottomSheetRef$curre7;

    if (minimized.current) {
      return;
    }

    minimized.current = true;
    /**
     * if modal got minimized before it finish its mounting
     * animation, we set the `restoreIndexRef` to the
     * provided index.
     */

    if (currentIndexRef.current === -1) {
      restoreIndexRef.current = index;
    } else {
      restoreIndexRef.current = currentIndexRef.current;
    }

    (_bottomSheetRef$curre7 = bottomSheetRef.current) === null || _bottomSheetRef$curre7 === void 0 ? void 0 : _bottomSheetRef$curre7.close();
  }, [index]);
  const handleRestore = useCallback(() => {
    var _bottomSheetRef$curre8;

    if (!minimized.current || forcedDismissed.current) {
      return;
    }

    minimized.current = false;
    (_bottomSheetRef$curre8 = bottomSheetRef.current) === null || _bottomSheetRef$curre8 === void 0 ? void 0 : _bottomSheetRef$curre8.snapTo(restoreIndexRef.current);
  }, []); //#endregion
  //#region callbacks

  const handlePortalOnUnmount = useCallback(() => {
    var _bottomSheetRef$curre9;

    /**
     * if modal is already been dismiss, we exit the method.
     */
    if (currentIndexRef.current === -1 && minimized.current === false) {
      return;
    }

    mounted.current = false;
    forcedDismissed.current = true;

    if (minimized.current) {
      unmount();
      return;
    }

    willUnmountSheet(key);
    (_bottomSheetRef$curre9 = bottomSheetRef.current) === null || _bottomSheetRef$curre9 === void 0 ? void 0 : _bottomSheetRef$curre9.close();
  }, [key, unmount, willUnmountSheet]);
  const handleBottomSheetOnChange = useCallback(_index => {
    const adjustedIndex = adjustIndex(_index);
    currentIndexRef.current = _index;

    if (_providedOnChange) {
      _providedOnChange(adjustedIndex);
    }

    if (minimized.current) {
      return;
    }

    if (adjustedIndex === -1) {
      unmount();
    }
  }, [adjustIndex, unmount, _providedOnChange]); //#endregion
  //#region expose public methods

  useImperativeHandle(ref, () => ({
    // sheet
    snapTo: handleSnapTo,
    expand: handleExpand,
    collapse: handleCollapse,
    close: handleClose,
    // modal
    present: handlePresent,
    dismiss: handleDismiss,
    // private
    minimize: handleMinimize,
    restore: handleRestore
  })); //#endregion
  // render

  return mount ? /*#__PURE__*/React.createElement(Portal, {
    key: key,
    name: key,
    handleOnUnmount: handlePortalOnUnmount
  }, /*#__PURE__*/React.createElement(BottomSheet, _extends({}, bottomSheetProps, {
    ref: bottomSheetRef,
    key: key,
    index: index,
    snapPoints: snapPoints,
    animateOnMount: true,
    topInset: topInset,
    bottomInset: bottomInset,
    containerHeight: safeContainerHeight,
    onChange: handleBottomSheetOnChange,
    children: children
  }))) : null;
});
const BottomSheetModal = /*#__PURE__*/memo(BottomSheetModalComponent, isEqual);
export default BottomSheetModal;
//# sourceMappingURL=BottomSheetModal.js.map