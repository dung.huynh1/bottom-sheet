function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { memo } from 'react';
import { View } from 'react-native';
import isEqual from 'lodash.isequal';
import { styles } from './styles';
import { DEFAULT_ACCESSIBLE, DEFAULT_ACCESSIBILITY_ROLE, DEFAULT_ACCESSIBILITY_LABEL, DEFAULT_ACCESSIBILITY_HINT } from './constants';

const BottomSheetHandleComponent = props => {
  const {
    accessible = DEFAULT_ACCESSIBLE,
    accessibilityRole = DEFAULT_ACCESSIBILITY_ROLE,
    accessibilityLabel = DEFAULT_ACCESSIBILITY_LABEL,
    accessibilityHint = DEFAULT_ACCESSIBILITY_HINT,
    ...rest
  } = props;
  return /*#__PURE__*/React.createElement(View, _extends({
    style: styles.container,
    accessible: accessible !== null && accessible !== void 0 ? accessible : undefined,
    accessibilityRole: accessibilityRole !== null && accessibilityRole !== void 0 ? accessibilityRole : undefined,
    accessibilityLabel: accessibilityLabel !== null && accessibilityLabel !== void 0 ? accessibilityLabel : undefined,
    accessibilityHint: accessibilityHint !== null && accessibilityHint !== void 0 ? accessibilityHint : undefined
  }, rest), /*#__PURE__*/React.createElement(View, {
    style: styles.indicator
  }));
};

const BottomSheetHandle = /*#__PURE__*/memo(BottomSheetHandleComponent, isEqual);
export default BottomSheetHandle;
//# sourceMappingURL=BottomSheetHandle.js.map