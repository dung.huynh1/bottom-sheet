import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  container: { ...StyleSheet.absoluteFillObject,
    overflow: 'hidden'
  }
});
//# sourceMappingURL=styles.js.map