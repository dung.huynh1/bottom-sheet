import React, { memo, useCallback, useMemo } from 'react';
import { View } from 'react-native';
import isEqual from 'lodash.isequal';
import { styles } from './styles';

const BottomSheetContainerComponent = ({
  shouldMeasureHeight,
  onMeasureHeight,
  children,
  topInset = 0,
  bottomInset = 0
}) => {
  //#region callbacks
  const handleOnLayout = useCallback(({
    nativeEvent: {
      layout: {
        height
      }
    }
  }) => {
    onMeasureHeight(height);
  }, [onMeasureHeight]); //#endregion

  const containerStyle = useMemo(() => [styles.container, {
    top: topInset,
    bottom: bottomInset
  }], [bottomInset, topInset]); //#region render
  // console.log('BottomSheetContainer', 'render', shouldMeasureHeight);

  return /*#__PURE__*/React.createElement(View, {
    pointerEvents: "box-none",
    style: containerStyle,
    onLayout: shouldMeasureHeight ? handleOnLayout : undefined,
    children: children
  }); //#endregion
};

const BottomSheetContainer = /*#__PURE__*/memo(BottomSheetContainerComponent, isEqual);
export default BottomSheetContainer;
//# sourceMappingURL=BottomSheetContainer.js.map