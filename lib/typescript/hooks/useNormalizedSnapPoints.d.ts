export declare const useNormalizedSnapPoints: (snapPoints: ReadonlyArray<number | string>, containerHeight?: number, handleHeight?: number) => number[];
