/// <reference types="react" />
import type { Scrollable, ScrollableType } from '../types';
export declare const useScrollableInternal: (type: ScrollableType) => {
    scrollableRef: import("react").RefObject<Scrollable>;
    handleOnBeginDragEvent: (...args: any[]) => void;
    handleOnContentSizeChange: (_: any, height: number) => void;
    handleSettingScrollable: () => () => void;
};
