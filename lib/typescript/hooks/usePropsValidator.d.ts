import type { BottomSheetProps } from '../components/bottomSheet';
export declare const usePropsValidator: ({ index, snapPoints, topInset, animationDuration, animationEasing, }: BottomSheetProps) => void;
