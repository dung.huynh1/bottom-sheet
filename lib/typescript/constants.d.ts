declare const WINDOW_HEIGHT: number, WINDOW_WIDTH: number;
declare enum GESTURE {
    UNDETERMINED = 0,
    CONTENT = 1,
    HANDLE = 2
}
export { GESTURE, WINDOW_HEIGHT, WINDOW_WIDTH };
