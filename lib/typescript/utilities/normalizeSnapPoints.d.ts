/**
 * Converts snap points with percentage to fixed numbers.
 */
export declare const normalizeSnapPoints: (snapPoints: ReadonlyArray<number | string>, containerHeight: number) => number[];
