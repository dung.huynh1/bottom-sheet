export declare const styles: {
    container: {
        backgroundColor: string;
        borderTopLeftRadius: number;
        borderTopRightRadius: number;
    };
};
