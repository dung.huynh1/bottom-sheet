import type { BackdropPressBehavior, BottomSheetDefaultBackdropProps } from './types';
export declare const usePressBehavior: ({ closeOnPress, disappearsOnIndex, pressBehavior, }: Pick<BottomSheetDefaultBackdropProps, 'closeOnPress' | 'disappearsOnIndex' | 'pressBehavior'>) => {
    handleOnPress: () => void;
    syntheticPressBehavior: BackdropPressBehavior;
};
