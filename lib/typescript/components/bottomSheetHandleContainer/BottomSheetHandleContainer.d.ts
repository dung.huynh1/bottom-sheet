import React from 'react';
import type { BottomSheetHandleContainerProps } from './types';
declare const BottomSheetHandleContainer: React.MemoExoticComponent<({ animatedIndex, animatedPosition, simultaneousHandlers, enableHandlePanningGesture, shouldMeasureHeight, handleComponent: _providedHandleComponent, onGestureEvent, onHandlerStateChange, onMeasureHeight, }: BottomSheetHandleContainerProps) => JSX.Element | null>;
export default BottomSheetHandleContainer;
