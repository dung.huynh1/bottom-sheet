import Animated from 'react-native-reanimated';
declare const DEFAULT_ANIMATION_EASING: Animated.EasingFunction;
declare const DEFAULT_ANIMATION_DURATION = 500;
declare const DEFAULT_ANIMATE_ON_MOUNT = false;
declare const DEFAULT_HANDLE_HEIGHT = 24;
declare const DEFAULT_ENABLE_CONTENT_PANNING_GESTURE = true;
declare const DEFAULT_ENABLE_HANDLE_PANNING_GESTURE = true;
declare const NORMAL_DECELERATION_RATE: number | undefined;
declare const DEFAULT_ACCESSIBLE = true;
declare const DEFAULT_ACCESSIBILITY_LABEL = "Bottom Sheet";
declare const DEFAULT_ACCESSIBILITY_ROLE = "adjustable";
declare const DEFAULT_ENABLE_ACCESSIBILITY_CHANGE_ANNOUNCEMENT = true;
declare const DEFAULT_ACCESSIBILITY_POSITION_CHANGE_ANNOUNCEMENT: (positionInScreen: string) => string;
export { DEFAULT_ANIMATION_EASING, DEFAULT_ANIMATION_DURATION, DEFAULT_ANIMATE_ON_MOUNT, DEFAULT_HANDLE_HEIGHT, DEFAULT_ENABLE_CONTENT_PANNING_GESTURE, DEFAULT_ENABLE_HANDLE_PANNING_GESTURE, NORMAL_DECELERATION_RATE, DEFAULT_ACCESSIBLE, DEFAULT_ACCESSIBILITY_LABEL, DEFAULT_ACCESSIBILITY_ROLE, DEFAULT_ENABLE_ACCESSIBILITY_CHANGE_ANNOUNCEMENT, DEFAULT_ACCESSIBILITY_POSITION_CHANGE_ANNOUNCEMENT, };
