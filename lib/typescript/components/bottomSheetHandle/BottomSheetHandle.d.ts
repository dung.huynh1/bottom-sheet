import React from 'react';
import type { BottomSheetDefaultHandleProps } from './types';
declare const BottomSheetHandle: React.MemoExoticComponent<(props: BottomSheetDefaultHandleProps) => JSX.Element>;
export default BottomSheetHandle;
