declare const DEFAULT_ACCESSIBLE = true;
declare const DEFAULT_ACCESSIBILITY_ROLE = "adjustable";
declare const DEFAULT_ACCESSIBILITY_LABEL = "Bottom Sheet handle";
declare const DEFAULT_ACCESSIBILITY_HINT = "Drag up or down to extend or minimize the Bottom Sheet";
export { DEFAULT_ACCESSIBLE, DEFAULT_ACCESSIBILITY_ROLE, DEFAULT_ACCESSIBILITY_LABEL, DEFAULT_ACCESSIBILITY_HINT, };
