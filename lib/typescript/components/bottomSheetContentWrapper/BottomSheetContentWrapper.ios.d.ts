import React from 'react';
import type { BottomSheetContentWrapperProps } from './types';
declare const BottomSheetContentWrapper: React.MemoExoticComponent<React.ForwardRefExoticComponent<BottomSheetContentWrapperProps & React.RefAttributes<React.ComponentType<import("react-native-gesture-handler").TapGestureHandlerProps & React.RefAttributes<any>>>>>;
export default BottomSheetContentWrapper;
